"custom/updates": {
    "format": "{} {icon}",
    "return-type": "json",
    "format-icons": {
        "has-updates": "󱍷",
        "updated": "󰂪"
    },
    "exec-if": "which waybar-module-pacman-updates",
    "exec": "waybar-module-pacman-updates --interval-seconds 5 --network-interval-seconds 300"
}

"custom/updates": {
    "format": "{} {icon}",
    "return-type": "json",
    "format-icons": {
        "has-updates": "󱍷",
        "updated": ""
    },
    "exec-if": "which waybar-module-pacman-updates",
    "exec": "waybar-module-pacman-updates --no-zero-output"
}

https://github.com/coffebar/waybar-module-pacman-updates

style.css

#custom-updates {
	background-color: transparent;
}

sh -c "$(wget -O- https://raw.githubusercontent.com/coffebar/waybar-module-pacman-updates/master/installer.sh)"
