BINDINGS

     spectrwm provides many functions (or actions) accessed via key or mouse bindings.

     The current mouse bindings are described below:

           M1            -   Focus window
           M-M1          -   Move window
           M-M3          -   Resize window
           M-S-M3        -   Resize window while maintaining it centered

     The default key bindings are described below:

           M-S-⟨Return⟩   -  term
           M-p            -  menu
           M-S-q          -  quit
           M-q            -  restart
           M-⟨Space⟩      -  cycle_layout
           M-S-\          -  flip_layout
           M-S-⟨Space⟩    -  stack_reset
           M-h            -  master_shrink
           M-l            -  master_grow
           M-,            -  master_add
           M-.            -  master_del
           M-S-,          - stack_inc
           M-S-.          -  stack_dec
           M-⟨Return⟩     -  swap_main
           M-j, M-⟨TAB⟩   -  focus_next
           M-k, M-S-⟨TAB⟩ -  focus_prev
           M-m            -  focus_main
           M-S-j          -  swap_next
           M-S-k          -  swap_prev
           M-b            -  bar_toggle
           M-x            -  wind_del
           M-S-x          -  wind_kill
           M-⟨n⟩          -  ws_n
           M-S-⟨n⟩        -  mvws_n
           M-⟨Right⟩      -  ws_next
           M-⟨Left⟩       -  ws_prev
           M-⟨Up⟩         -  ws_next_all
           M-⟨Down⟩       -  ws_prev_all
           M-a            -  ws_prior
           M-S-⟨Right⟩    -  screen_next
           M-S-⟨Left⟩     -  screen_prev
           M-s            -  screenshot_all
           M-S-s          -  screenshot_wind
           M-S-v          -  version
           M-t            -  float_toggle
           M-S-⟨Delete⟩   -  lock
           M-S-i          -  initscr
           M-w            -  iconify
           M-S-w            uniconify
           M-S-r            always_raise
           M-v              button2
           M--              width_shrink
           M-=              width_grow
           M-S--            height_shrink
           M-S-=            height_grow
           M-[              move_left
           M-]              move_right
           M-S-[            move_up
           M-S-]            move_down
           M-S-/            name_workspace
           M-/              search_workspace
           M-f              search_win

     The action names and descriptions are listed below:

           term            -  Spawn a new terminal (see PROGRAMS above).
           menu            -  Menu (see PROGRAMS above).
           quit            -  Quit spectrwm.
           restart         -  Restart spectrwm.
           cycle_layout    -  Cycle layout.
           flip_layout     -  Swap the master and stacking areas.
           stack_reset     - Reset layout.
           master_shrink   -  Shrink master area.
           master_grow     -  Grow master area.
           master_add      -  Add windows to master area.
           master_del      -  Remove windows from master area.
           stack_inc       -  Add columns/rows to stacking area.
           stack_dec       -  Remove columns/rows from stacking area.
           swap_main       -  Move current window to master area.
           focus_next      -  Focus next window in workspace.
           focus_prev      -  Focus previous window in workspace.
           focus_main      -  Focus on main window in workspace.
           swap_next       -  Swap with next window in workspace.
           swap_prev       -  Swap with previous window in workspace.
           bar_toggle      -  Toggle status bar in all workspaces.
           wind_del        -  Delete current window in workspace.
           wind_kill       -  Destroy current window in workspace.
           ws_n            -  Switch to workspace n, where n is 1 through 10.
           mvws_n          -  Move current window to workspace n, where n is 1 through 10.
           ws_next         -  Switch to next workspace with a window in it.
           ws_prev         -  Switch to previous workspace with a window in it.
           ws_next_all     -  Switch to next workspace.
           ws_prev_all     -  Switch to previous workspace.
           ws_prior        -  Switch to last visited workspace.
           screen_next     -  Move pointer to next region.
           screen_prev     -  Move pointer to previous region.
           screenshot_all  -  Take screenshot of entire screen (if enabled) (see PROGRAMS above).
           screenshot_wind -  Take screenshot of selected window (if enabled) (see PROGRAMS
                             above).
           version         -  Toggle version in status bar.
           float_toggle    -  Toggle focused window between tiled and floating.
           lock            -  Lock screen (see PROGRAMS above).
           initscr         -  Reinitialize physical screens (see PROGRAMS above).
           iconify         -  Minimize (unmap) currently focused window.
           uniconify       -  Maximize (map) window returned by dmenu selection.
           always_raise    -  When set tiled windows are allowed to obscure floating windows.
           button2         -  Fake a middle mouse button click (mouse button 2).
           width_shrink    -  Shrink the width of a floating window.
           width_grow      -  Grow the width of a floating window.
           height_shrink   -  Shrink the height of a floating window.
           height_grow     -  Grow the height of a floating window.
           move_left       -  Move a floating window a step to the left.
           move_right      -  Move a floating window a step to the right.
           move_up         -  Move a floating window a step upwards.
           move_down       -  Move a floating window a step downwards.
           name_workspace  -  Name the current workspace.
           search_workspace -  Search for a workspace.
           search_win      -  Search the windows in the current workspace.
           
     Custom bindings in the configuration file are specified as follows:

           bind[<action>] = <keys>

     ⟨action⟩ is one of the actions listed above (or empty) and ⟨keys⟩ is in the form of zero or
     more modifier keys (MOD, Mod1, Shift, etc.) and one or more normal keys (b, space, etc.),
     separated by "+".  For example:

           bind[reset] = Mod4+q # bind Windows-key + q to reset
           bind[] = Mod1+q # unbind Alt + q

     To use the currently defined modkey, specify MOD as the modifier key.

     Multiple key combinations may be bound to the same action.           
