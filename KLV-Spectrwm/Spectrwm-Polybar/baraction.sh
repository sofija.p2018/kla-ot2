#!/bin/bash
# Example Bar Action Script for Linux.
# Requires: acpi,  lm_sensors, 
# 

hostname="${HOSTNAME:-${hostname:-$(hostname)}}"

##############################
#	    CPU
##############################
cpu() {
	  read cpu a b c previdle rest < /proc/stat
	    prevtotal=$((a+b+c+previdle))
	      sleep 0.5
	        read cpu a b c idle rest < /proc/stat
		  total=$((a+b+c+idle))
		    cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
            echo "CPU: $cpu%"
	      }
##############################
#	    RAM
##############################
mem() {
used="$(free | grep Mem: | awk '{print $3}')"
total="$(free | grep Mem: | awk '{print $2}')"
human="$(free -h | grep Mem: | awk '{print $3}' | sed s/i//g)"

ram="$(( 200 * $used/$total - 100 * $used/$total ))% ($human) "

echo "RAM: $ram"
}

##############################
#	    VOLUME
##############################
vol() {
    vol=`amixer get Master | awk -F'[][]' 'END{ print $4":"$2 }' | sed 's/on://g'`
    echo -e "VOL: $vol"
}

##############################
#	    CLOCK
##############################
#dateinfo() {
#    echo "$(date "+%b %d %Y (%a)")"
#}

clockinfo() {
    echo $(date "+%H:%M:%S")
}

# Loop to update bar output
update(){
	echo " ⋘$(hostname)⋙       +@fg=5; +@bg=3;+@fg=1; $(cpu) +@fg=4; +@bg=2;+@fg=1; $(mem) +@fg=7; +@bg=5;+@fg=1; $(vol) +@fg=8; +@bg=6;+@fg=1; $(clockinfo) +@fg=1; +@bg=0;"
	
	#echo " ⋘$(hostname)⋙ +@fg=5; ◀+@bg=3;+@fg=1; $(cpu) +@fg=4; ◀+@bg=2;+@fg=1; $(mem) +@fg=7; ◀+@bg=5;+@fg=1; $(vol) +@fg=8; ◀+@bg=6;+@fg=1; $(clockinfo) +@fg=1; ◀+@bg=0;"
	
	#echo " $(cpu)+@fg=3; +@bg=2;+@fg=1;  $(mem)  +@fg=4;+@bg=2;+@fg=1;  $(hdd) +@fg=5;+@bg=3;+@fg=1; $(vol) +@fg=7;+@bg=5;+@fg=1; $(bat) +@fg=8;+@bg=6;+@fg=1; $(network) +@fg=1;+@bg=0;"
    wait
}
while :; do
		update
		#~/.config/spectrwm/scripts/trayer_follows.sh
    sleep 2 &
    wait
done

SLEEP_SEC=1
#loops forever outputting a line every SLEEP_SEC secs
# echo "$(cpu) | $(mem) | $(pkgs) | $(upgrades) | $(hdd) | $(vpn) $(network) | $(vol) | $(WEATHER) $(TEMP) "

while :; do
echo "+@fg=1; $(cpuicon) +@fg=0; $(cpu) +@fg=1; $(memicon) +@fg=0; $(mem) +@fg=1; $(volicon) +@fg=0; $(vol) +@fg=1; $(dateinfo) +@fg=4; $(clockicon) +@fg=0; $(clockinfo)  "

sleep $SLEEP_SEC

done
