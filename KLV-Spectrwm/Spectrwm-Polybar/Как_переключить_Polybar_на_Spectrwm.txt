Оставьте оригинальный бар включенным с минимальными настройками, и удалите зарезервированное пространство для Polybar, если оно было настроено ранее (region в конфиге). Вам понадобятся только следующие пункты конфигурации бара Spectrwm:
bar_enabled     = 1
bar_border_width    = 0
bar_font        = <any font, just adjust size so bar is as large as Polybar>
bar_format      = ' # <--- something minimal so bar is almost not visible

убедитесь, что ваша оригинальная привязка клавиш для переключения панели Spectrwm все еще активна (Super+b)

установите xdotool. Миниатюрная программа, которая может посылать поддельные ключи в Xorg

напишите сценарий оболочки, я назвал его fullscreen-toggle.sh и поместите его куда-нибудь (мои находятся в ~/scripts/). Также сделайте его исполняемым:

#!/bin/sh
xdotool key super+b
polybar-msg cmd toggle


Скрипт очень прост. Сначала xdotool переключает панель Spectrwm, посылая фальшивые нажатия клавиш. Затем мы используем polybar-msg для отправки удаленной команды на Polybar для переключения.

Добавьте новую привязку клавиш в Spectrwm conf (~/.spectrwm.comf). Я использовал MOD+Shift+f:

program[toggle_bars] = ~/scripts/fullscreen-toggle.sh
bind[toggle_bars] = MOD+Shift+f


… и все готово! Перезапустите Spectrwm (Super+q) и попробуйте. Убедитесь, что ваша привязка клавиш была свободна перед назначением. Также попробуйте запустить shell-скрипт из терминала для проверки.

