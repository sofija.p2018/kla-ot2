#!/bin/bash
## wd_initconfig_make
# Determine FirstRib grub stanzas for menu.lst and grub.conf
# Revision Date: 05Apr2024
# Copyright wiak (William McEwan) 30 May 2022+; Licence: MIT

## Run this script from your desired upper_changes directory

progname="wd_initconfig_make"; version="2.0.0"; revision="-rc2"

case "$1" in
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
	'-h'|'--help'|'-?') printf "Run this script from your desired upper_changes directory with command:
./wd_initconfig_make  # if bootdir is of the form /dir1
or as
./wd_initconfig_make 2  # if bootdir is of the form /dir1/dir2
or as
./wd_initconfig_make 3  # if bootdir is of the form /dir1/dir2/dir3\n";exit 0;;
	"-*") printf "option $1 not available\n";exit 0;;
esac

_grub_config_override (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "w_changes=UUID=${bootuuid}=/$subdir
w_changes1=RAM2
# Note that you can remove w_changes1=RAM2 if you don't want
save session on demand mode and prefer direct writes.\n" | tee w_initconfig
}

HERE="`pwd`"
[ "$1" = "" ] && subdirs=1 || subdirs=$1
case $subdirs in
1) level2=""
level1=`basename "$HERE"`
;;
2) level2=`basename "$HERE"`
path_level1=`dirname "$HERE"`
level1=`basename "$path_level1"`/
;;
3) level3=`basename "$HERE"`
path_level2=`dirname "$HERE"`
level2=`basename "$path_level2"`/
path_level1=`dirname "$path_level2"`
level1=`basename "$path_level1"`/
;;
esac
bootdir="${level1}""${level2}""${level3}" # for use with grub config override
_grub_config_override
exit 0

