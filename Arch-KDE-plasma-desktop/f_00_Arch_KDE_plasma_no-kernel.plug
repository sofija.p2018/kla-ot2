###### f_00_Arch_KDE_plasma_no-kernel.plug version 1.0.0-r1
# Revision Date: 01.11.2024
# Copyright wiak (William McEwan) 30 May 2019+; Licence MIT (aka X11 license)
#    for use with build_firstrib_rootfs_XXX.sh to build:
# ./build_firstrib_rootfs.sh arch default amd64 f_00_Arch_KDE_plasma_no-kernel.plug 

# Some clean up from previous runs in case build_firstrib_rootfs or
# make00<distro>XXX being re-run to fetch packages that failed owing
# to repo timeouts or whatever (wiak remove: this and some other tweaks in script below may not be enough to guarantee correct second run - not sure):
[ -d /etc/skel ] && rm -rf /etc/skel
userdel -f firstrib
[ -d /home/firstrib ] && rm -rf /home/firstrib
userdel -f spot
[ -d /home/spot ] && rm -rf /home/spot

### START package manager install---------------------------------------
# Note ESSENTIAL; need full wget since busybox wget can't handle https
pacman -Syu
echo wget \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

## EXPERIMENTAL: Force pacman to use wget -c rather than it's default mode to make more robust against timeouts - wiak remove NO LONGER SEEMS TO WORK SO COMMENTING OUT
#if ! grep -q '^XferCommand = /usr/bin/wget' /etc/pacman.conf; then
# sed -i '/XferCommand = \/usr\/bin\/wget/a XferCommand = /usr/bin/wget --passive-ftp -c -O %o %u' /etc/pacman.conf
#fi

## install some MAIN packages including wpa_supplicant and busybox for wifi connectivity
# PROBABLY BEST TO TREAT AS ESSENTIAL
echo tzdata systemd-sysvcompat dosfstools mtools procps-ng which wpa_supplicant busybox ntfs-3g \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

# Make some network-related applet symlinks for above busybox
cd /usr/bin; ln -s busybox ash; ln -s /usr/lib/systemd/systemd init  # wiak remove: recently seemed to need this link to systemd
# Hopefully not required...: ln -s busybox ip; ln -s busybox route; ln -s busybox ifconfig; ln -s busybox ping; ln -s busybox udhcpc;
cd ../..

## install OPTIONAL KERNEL/MODULES/FIRMWARE
# NOTE: leave linux and linux-firmware out for huge kernel builds such as kernel from KLV-Airedale:
# When pacman installs linux this scripts uses default mkinitcpio option
# Also provides modules optionally required by later FirstRib initramfs build
#echo linux linux-firmware sof-firmware \
#| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout
echo sof-firmware \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

# Ignore kernel upgrade during future pacman -Syu
# Note: if desired to upgrade kernel+related-modules you also have to do change the modules used by initrd as well.
# Easy if using huge kernel KLA-type mechanism though - otherwise need to uncompress/recompress initrd
# Surrounding such sed lines with if check, so only done once if script re-run - wiak: to check such measures throughout script
if ! grep -q 'IgnorePkg = linux' /etc/pacman.conf; then
  sed -i '/#IgnorePkg/ a IgnorePkg = linux' /etc/pacman.conf
fi

## install NON-GUI APPS SELECTION
# Changing this selection may require you to also edit desktop apps menu
# PROBABLY BEST TO TREAT AS ESSENTIAL
# Changelog from rc2; user can optionally re-install: base-devel 
# nitrogen (установка обоев) для терминала zsh (zsh-autosuggestions zsh-syntax-highlighting)
 
echo sudo xdg-utils pciutils cpio zstd iproute2 file exfatprogs \
squashfs-tools usleep usbutils gettext jq geany mtpaint \
pipewire wireplumber pamixer pipewire-pulse pipewire-alsa qpwgraph \
bluez bluez-utils bluez-deprecated-tools blueman \
mpv smplayer gufw cups cups-pdf git cmake avahi btop \
gvfs gvfs-smb gvfs-mtp xterm xdialog yad leafpad ttf-dejavu xdotool \
jq unrar zip unzip p7zip ibus ark kcalc gwenview kcolorchooser \
kdialog ocean-sound-theme khelpcenter ffmpegthumbs \
xdg-user-dirs xdg-utils \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

## install and configure XORG related components // gxmessage xorg-xmessage
#  xf86-video-intel got rid of boot delay for me, but only worked with native resolution and sometimes hung
echo xorg xorg-xinit xorg-xmessage \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

#if [ ! -e ~/.xinitrc ]; then
#  sed -i '/xterm/d;/xclock/d' /etc/X11/xinit/xinitrc  # remove startup xterms/xclock
#  cp /etc/X11/xinit/xinitrc ~/.xinitrc    # into root home dir for editing
#fi

# /home/spot/.Xresources for larger xterm uxterm fonts
cat <<'XRESOURCES' > /home/spot/.Xresources
Xft*antialias: true
Xft*autohint: true
XTerm*background: black
XTerm*foreground: grey
XTerm*cursorColor: grey
XTerm.vt100.geometry: 95x25+150
XTerm.vt100.scrollBar: true
XTerm.vt100.scrollbar.width: 8
XTerm*faceName: Monospace Regular 
XTerm*faceSize: 9

UXft*antialias: true
UXft*autohint: true
UXTerm*background: #002b36
UXTerm*foreground: #839496
UXTerm*cursorColor: grey
UXTerm.vt100.geometry: 84x25+150
UXTerm*faceName: Monospace Regular 
UXTerm*faceSize: 8
XRESOURCES

## Create /root/ directories
#
mkdir -p /root/Desktop
mkdir -p /root/Documents
mkdir -p /root/Downloads
mkdir -p /root/Music
mkdir -p /root/my-applications
mkdir -p /root/Pictures
mkdir -p /root/Public
mkdir -p /root/Startup
mkdir -p /root/Templates
mkdir -p /root/Videos

# Create /home/spot directories
#
mkdir -p /home/spot/Desktop
mkdir -p /home/spot/Documents
mkdir -p /home/spot/Downloads
mkdir -p /home/spot/Music
mkdir -p /home/spot/my-applications
mkdir -p /home/spot/Pictures
mkdir -p /home/spot/Public
mkdir -p /home/spot/Startup
mkdir -p /home/spot/Templates
mkdir -p /home/spot/Videos

## install DESKTOP MANAGER
# echo plasma-desktop kde-applications-meta xorg kio-admin sddm \
echo plasma-meta kde-applications-meta kio-admin sddm \
| xargs -n1 pacman -S --noconfirm --needed --disable-download-timeout

## install EXTRA APPS GUI-MAINLY SELECTION
# font-manager,gucharmap (управление шрифтами)

### START GENERAL CONFIGURATION-----------------------------------------
pwconv # set up passwd system
grpconv
# Quietly set default spot passwd to "spot"
printf "spot\nspot" | passwd spot >/dev/null 2>&1

## START config systemd, basic wifi, X, Desktop, and sound--------------

# Create systemd override to achieve spot autologin (per Arch Wiki)
# Remove this section if not wanting autologin of spot
##mkdir -p /etc/systemd/system/getty@tty1.service.d/
##cat <<'AUTOLOGIN' >/etc/systemd/system/getty@tty1.service.d/override.conf
##[Service]
##ExecStart=
##ExecStart=-/usr/bin/agetty --autologin spot --noclear %I $TERM
##AUTOLOGIN

# enable NetworkManager
systemctl enable NetworkManager.service
#systemctl enable NetworkManager

# Arrange to startx in user's .bash_profile (per Arch Wiki)
# Remove this section if not wanting boot straight into X
touch /home/spot/.bash_profile
#if ! grep -q 'graphical.target' ~/.bash_profile; then
#cat <<'USERBASHPROFILE' >> /home/spot/.bash_profile
#if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
#  exec startx
#fi
#USERBASHPROFILE
#fi

touch /home/spot/.bashrc
cat <<'USERBASHPROFILE' >> /home/spot/.bashrc
#
# /etc/bash.bashrc
#

#pfetch
#fastfetch

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

# PS1="\[\e[0;36m\]┌──\[\e[0m\][ \[\e[0;33m\]\u\[\e[0m\]\[\e[0;32m\]@\[\e[0;36m\]\h\[\e[0m\] ] [ \[\e[0;36m\]\t\[\e[0m\] ]\n\[\e[0;36m\]├── \[\e[0;32m\]\w\[\e[0;36m\]\n\[\e[0;36m\]└>\[\e[0m\] "

case ${TERM} in
  Eterm*|alacritty*|aterm*|foot*|gnome*|konsole*|kterm*|putty*|rxvt*|tmux*|xterm*)
    PROMPT_COMMAND+=('printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"')

    ;;
  screen*)
    PROMPT_COMMAND+=('printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"')
    ;;
esac

if [[ -r /usr/share/bash-completion/bash_completion ]]; then
  . /usr/share/bash-completion/bash_completion
fi

#if [[ $(ps --no-header --pid=$PPID --format=comm) != "fish" && -z ${BASH_EXECUTION_STRING} ]]
#then
#	exec fish
#fi
USERBASHPROFILE

# Change to fancy prompt via .bashrc
VAR='PS1="\[\e[0;36m\]┌──\[\e[0m\][ \[\e[0;33m\]\u\[\e[0m\]\[\e[0;32m\]@\[\e[0;36m\]\h\[\e[0m\] ] [ \[\e[0;36m\]\t\[\e[0m\] ]\n\[\e[0;36m\]├── \[\e[0;32m\]\w\[\e[0;36m\]\n\[\e[0;36m\]└>\[\e[0m\]"'
{ printf '%s\n' "$VAR";cat /etc/bash.bashrc; } | sed '1{h;d;};/PS1=/g' > /home/spot/.bashrc

#-----------------------------------------------------------------------

# The following may need updated for newer glibc versions?
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/core_resources/save2flash_fr-latest.tar.xz -O /save2flash_fr-latest.tar.xz
tar xJf /save2flash_fr-latest.tar.xz && rm /save2flash_fr-latest.tar.xz

#-----------------------------------------------------------------------

## Create and switch to build directory
cd /var/cache/pacman/pkg
wget -c https://gitlab.com/sofija.p2018/kla-ot2/-/raw/main/libpamac-aur-11.6.4-6-x86_64.pkg.tar.zst 
wget -c https://gitlab.com/sofija.p2018/kla-ot2/-/raw/main/pamac-aur-11.7.1-5-x86_64.pkg.tar.zst
echo libpamac-aur-11.6.4-6-x86_64.pkg.tar.zst pamac-aur-11.7.1-5-x86_64.pkg.tar.zst \
| xargs -n1 pacman -U --noconfirm --needed --disable-download-timeout

### END usrlocalbin scripts---------------------------------------------

# Configure system for multi-users
#
cp -af /root/. /etc/skel
mkdir -p /etc/skel/.config /etc/skel/.cache /etc/skel/.local/share
#
# Give wheel group nopasswd sudo rights and create firstrib as wheel group member
# PREV (was wrong I think...): groupadd sudo && 
echo '%wheel ALL=(ALL) NOPASSWD: ALL' | (VISUAL="tee -a" visudo) # wheel group added to sudo no password required
useradd -m -G audio,video,wheel,storage -s /bin/bash firstrib  # firstrib in wheel group so has elevated sudo permissions
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"

# Create user spot and put in wheel group (and more) and give wheel group nopasswd sudo rights
echo '%wheel ALL=(ALL) NOPASSWD: ALL' | (VISUAL="tee -a" visudo) # wheel group added to sudo no password required
useradd -m -G audio,video,wheel,storage -s /bin/bash spot
printf "spot\nspot" | passwd spot >/dev/null 2>&1 # Quietly set default spot passwd to "spot"

##----------------------------------------------------------------------

# Restore busybox network-related links if required
# If required the following should, I think, be done at very end of 
# plugin to avoid conflicts with any pacman network-related app installs
cd /usr/bin; ln -s busybox ip; ln -s busybox route; ln -s busybox ifconfig; ln -s busybox arp; ln -s busybox ping; ln -s busybox udhcpc
cd ../..

# Empty pacman pkg cache
printf "%s\n%s\n" y y | pacman -Scc

# Avahi-daemon autostart
# enable systemd avahi-daemon.service / avahi-daemon.socket / dbus-org.freedesktop.Avahi.service
ln -s /usr/lib/systemd/system/avahi-daemon.service /etc/systemd/system/multi-user.target.wants/
ln -s /usr/lib/systemd/system/avahi-daemon.service /etc/systemd/system/dbus-org.freedesktop.Avahi.service
mkdir -p /etc/systemd/system/sockets.target.wants
ln -s /usr/lib/systemd/system/avahi-daemon.socket /etc/systemd/system/sockets.target.wants/

# systemctl enable cups.service / systemctl start cups.service
mkdir -p /etc/systemd/system/printer.target.wants
ln -s /usr/lib/systemd/system/cups.service /etc/systemd/system/printer.target.wants/cups.service
ln -s /usr/lib/systemd/system/cups.service /etc/systemd/system/multi-user.target.wants/cups.service
ln -s /usr/lib/systemd/system/cups.socket /etc/systemd/system/sockets.target.wants/cups.socket
ln -s /usr/lib/systemd/system/cups.path /etc/systemd/system/multi-user.target.wants/cups.path

# pipewire pipewire-pulse wireplumber - service
# systemctl enable --user pipewire pipewire-pulse wireplumber 
mkdir -p /home/spot/.config/systemd/user
mkdir -p /home/spot/.config/systemd/user/default.target.wants
ln -s /usr/lib/systemd/user/pipewire.service /home/spot/.config/systemd/user/default.target.wants/pipewire.service
mkdir -p /home/spot/.config/systemd/user/sockets.target.wants
ln -s /usr/lib/systemd/user/pipewire.socket /home/spot/.config/systemd/user/sockets.target.wants/pipewire.socket
ln -s /usr/lib/systemd/user/pipewire-pulse.service /home/spot/.config/systemd/user/default.target.wants/pipewire-pulse.service
ln -s /usr/lib/systemd/user/pipewire-pulse.socket /home/spot/.config/systemd/user/sockets.target.wants/pipewire-pulse.socket
ln -s /usr/lib/systemd/user/wireplumber.service /home/spot/.config/systemd/user/pipewire-session-manager.service
mkdir -p /home/spot/.config/systemd/user/pipewire.service.wants
ln -s /usr/lib/systemd/user/wireplumber.service /home/spot/.config/systemd/user/pipewire.service.wants/wireplumber.service

# Bluetooth (systemctl enable bluetooth)
# add blueman to autostart and activate systemd bluetooth module
ln -s /usr/lib/systemd/system/bluetooth.service /etc/systemd/system/dbus-org.bluez.service
mkdir -p /etc/systemd/system/bluetooth.target.wants
ln -s /usr/lib/systemd/system/bluetooth.service /etc/systemd/system/bluetooth.target.wants/bluetooth.service 

# systemctl enable sfs-load-run  /  systemctl enable sfs-load-finish
#ln -s /usr/lib/systemd/system/sfs-load-run.service /etc/systemd/system/multi-user.target.wants/sfs-load-run.service
#mkdir -p /etc/systemd/system/final.target.wants
#ln -s /usr/lib/systemd/system/sfs-load-finish.service /etc/systemd/system/final.target.wants/sfs-load-finish.service

# systemctl enable ufw 
ln -s /usr/lib/systemd/system/ufw.service /etc/systemd/system/multi-user.target.wants/ufw.service

systemctl enable saveatshutdown.service
ln -s /usr/lib/systemd/system/saveatshutdown.service /etc/systemd/system/multi-user.target.wants/saveatshutdown.service

# Enable sddm / systemctl enable sddm.service
ln -s /usr/lib/systemd/system/sddm.service /etc/systemd/system/display-manager.service

# TIMEZONE SETUP
# enable systemd systemd-timesyncd.service
mkdir -p /etc/systemd/system/sysinit.target.wants
ln -s /usr/lib/systemd/system/systemd-timesyncd.service /etc/systemd/system/sysinit.target.wants/systemd-timesyncd.service
current_timezone="Etc/UTC"
ln -sf /usr/share/zoneinfo/${current_timezone} /etc/localtime
printf "\nCurrent timezone is: %s\n" ${current_timezone}
printf "To change timezone, open a terminal and enter the command: tzselect\n"
