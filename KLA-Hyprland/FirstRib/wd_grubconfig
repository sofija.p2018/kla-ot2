#!/bin/bash
## wd_grubconfig
# Determine FirstRib grub stanzas for menu.lst and grub.conf
# Revision Date: 19 Sep 2023
# Copyright wiak (William McEwan) 30 May 2022+; Licence: MIT

## Run this script from your FirstRib distro installation directory

progname="wd_grubconfig"; version="2.0.0"; revision="-rc2"

case "$1" in
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
	'-h'|'--help'|'-?') printf "Run this script from your FirstRib distro installation directory with command:
./wd_grubconfig  # if bootdir is of the form /dir1
or as
./wd_grubconfig 2  # if bootdir is of the form /dir1/dir2
or as
./wd_grubconfig 3  # if bootdir is of the form /dir1/dir2/dir3\n";exit 0;;
	"-*") printf "option $1 not available\n";exit 0;;
esac

_grub_config (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
Assuming names: kernel is vmlinuz and initrd is initrd.gz and booting is
from this build directory and needed modules and firmware are present:
"
if [ "$bootiso" != '*.iso' ];then
isotext="
#####grub.cfg (BOOT from ISO, note the LABEL or UUID options below):
menuentry \"${subdir} ISO LABEL ${bootlabel}\" {
  echo \"Searching partitions for iso. Please wait patiently...\"
  set isopath=/${subdir}/${bootiso}
  search --no-floppy --file --set=root \$isopath
  loopback loop \$isopath
  set root=(loop)
  linux (loop)/vmlinuz w_bootfrom=\$isopath w_changes=LABEL=${bootlabel}=/${subdir} w_changes1=RAM2
  initrd (loop)/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir} ISO UUID\" {
  echo \"Searching partitions for iso. Please wait patiently...\"
  set isopath=/${subdir}/${bootiso}
  search --no-floppy --file --set=root \$isopath
  loopback loop \$isopath
  set root=(loop)
  linux (loop)/vmlinuz w_bootfrom=\$isopath w_changes=${bootuuid}=/${subdir} w_changes1=RAM2
  initrd (loop)/initrd.gz
}"
fi
printf "$isotext

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}

Refer to $PWD/grub_config.txt for
copy of this information plus blkid info.
Note that you can remove w_changes=RAM2 if you don't want
save session on demand mode.\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
}

HERE="`pwd`"
[ "$1" = "" ] && subdirs=1 || subdirs=$1
case $subdirs in
1) level2=""
level1=`basename "$HERE"`
;;
2) level2=`basename "$HERE"`
path_level1=`dirname "$HERE"`
level1=`basename "$path_level1"`/
;;
3) level3=`basename "$HERE"`
path_level2=`dirname "$HERE"`
level2=`basename "$path_level2"`/
path_level1=`dirname "$path_level2"`
level1=`basename "$path_level1"`/
;;
esac
bootdir="${level1}""${level2}""${level3}" # for use with grub config
for bootiso in *.iso; do :;done
_grub_config
exit 0

