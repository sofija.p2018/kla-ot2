# Welcome to my Hyprland help, and tips and tricks 

  Super = Windows Key

# common operations
  Super          H        *keyhint* (THIS DOCUMENT)
  Super          Return   *term* (`foot`)
  Super          Q        *quit* (kill focused window)
  Super   Shift  Q        *quit* (kill focused window)
  Super          D        *show app menu* (`wofi small`)
  Super   Shift  D        *show app menu* (`wofi large`)
  Super   SHIFT  N        *Network configuration       
  Super   SHIFT  F        *Pcmanfm (root)      
  Super   SHIFT  G        *Geany (root)     
  Super   SHIFT  T        *Sakura (root)
  Super   SHIFT  Y        *Restarting the panel
  Super   SHIFT  P        *Preservation
  Super   SHIFT  X        *Preservation  
  Super   SHIFT  Space    *Toggle floating 
  Super   SHIFT  Return   *Sakura Terminal
  Super   CTRL   W        *Selecting a panel style
  Super   SHIFT  B        *Hide waybar
  Super   SHIFT  O        *Pipette - color selection
  Super   SHIFT  W        *Screen video recording start/stop 
  Super   CTRL   G        *Gparted     
  Super   ALT    V        *Clipboard - Wofi
  Super   ALT    R        *Dark Light Wofi
  Super   ALT    B        *Rainbow Borders
  Super   ALT    X        *Sunset - like automatic Redshift mode
  Super   ALT    J        *Choosing Wofi Emoji
  Super   ALT    P        *WoFi Power Menu
  Super   ALT    T        *Toggleallfloat
  Super   ALT    E        *QuickEdit
  Super   ALT    G        *Glassmorphism blur
  Super   ALT    Menu     *Airplane mode
  Super   ALT    K        *Clock Terminal  
  Super          M        *Dispatch splitratio 0.3
  Super          P        *Pseudo, # shrink window
  Super          F        *full screen window mode
  Super          C        *clear cliphist  
  
  **********************************************************************
  Super          Space    *changeLayout   #Enable to use additional keys

  Super          Escape   *hyprctl kill
  Super          O        *togglespl 
  Super          P        *pseudo, # dwindle   
  Super          J        *cyclenext
  Super          K        *cycleprev

  **********************************************************************
 
# touchpad
  Menu,   Touchpad.sh     # enable / disable touchpad

# wallpaper / styling stuff
  Super           w       *wallpaper shuffle* (right click on wallpaper   waybar module)
  Super   Ctrl    w       *wallpaper switcher* (click on wallpaper waybar    module)
  
  - To change permanently the wallpaper edit the file in *~/.config/hypr/configs/Execs.conf*
  - For a persistent wallpaper after dark-light mode, edit your Execs.conf. Either delete or put # before exec-once=swww query | swww init and delete the # before exec-once = swww init (Lines 7 and 8 on Execs.conf )

# Monitor, executables, keybindings, window rules, 
  files are located in *~/.config/hypr/configs*
  Keybindings file is located here *~/.config/hypr/configs/Keybinds.conf*

# screenshot may need to hold down the function (`fn`) key. You can change keybinds in *~/.config/hypr/configs/Keybinds.conf* 
  Super PrintScr(button)       *full screenshot*
  Super Shift PrintSrc(button) *active window screenshot*         
  Super CTRL SHIFT PrintScr    *full screenshot + timer (5s)*
  Super Alt PrintScr           *full screenshot + timer (10s)*
  Super Shift S                *screenshot with swappy*

# clipboard manager (cliphist)
  Super Alt V   *launch the wofi menu of clipboard manager* 
    - double click to select the clipboard. And paste as normal
    - to clean up clipboard manager, launch foot (super enter) then type cliphist wipe
    
# container layout
  Super   Shift   Space       *toggle tiling/floating mode*
  Super   left mouse button   *move window*
  Super   right mouse button  *resize window* (note only in float mode)

# workspaces
  Super         1 .. 0    *switch to workspace 1 .. 10*
  Super  Shift  1 .. 0    *move container to workspace 1 .. 10*
  Super   Tab             *cycle through workspaces*

# waybar customizations
  - waybar font too big or too small. Edit the font-size in waybar styles located in ~/.config/hypr/waybar/styles/ . By default, it is set to 100%. After adjusting the GTK font scaling to your liking, edit all the waybar styles. Reduce or increase according to your needs. NOTE that its on percent %. You can also change to px whichever suits you.

  - if you want 12h format instead of 24H format, edit the ~/.config/hypr/waybar/modules look for clock. delete the // and add // or delete the previous one

  - CPU Temperature:
    - a.) to change from deg C to deg F , edit the ~/.config/hypr/waybar/modules look for "temperature". Change the format to "format": "{temperatureF}°F {icon}",
    - b.) to fix the temperature if not showing correctly, comment "thermal zone": 0 by putting // before. Delete the // on the "hwmon path". Refresh waybar by pressing CTRL SHIFT w. If still not showing correctly, navigate to /sys/class/hwmon/ and open each hwmon. Look for k10temp for amd. Not sure about intel cpu. and edit accordingly the hwmon path in the "temperature" waybar module.
    - b.1) use this function to easily identify the hwmon path. Ran this in your terminal    ``` for i in /sys/class/hwmon/hwmon*/temp*_input; do echo "$(<$(dirname $i)/name): $(cat ${i%_*}_label 2>/dev/null || echo $(basename ${i%_*})) $(readlink -f $i)"; done ```
  
# Hyprland configurations
  - *Hyprland* configuration files are in `~/.config/hypr/`
  - files located in this folder can be edited using editor of your choice.

# notes for nvidia gpu users
  - Do note that you need to enable or disable some items in environment.conf file located in `~/.config/hypr/configs/environment.conf`


TO CLOSE THIS DOCUMENT - Super q or Super Shift q or if vim, press esc :q!
