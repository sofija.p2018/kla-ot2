#!/bin/bash

if pgrep xcompmgr &>/dev/null; then
    echo "Turning xcompmgr OFF"
    notify-send -a 'Turning xcompmgr' 'Xcompmgr OFF'
    pkill xcompmgr &
else
    echo "Turning xcompmgr ON"
    notify-send -a 'Turning xcompmgr' 'Xcompmgr ON'
    xcompmgr -C -t-5 -l-5 -r4.2 -o.55 &
fi

exit 0
